Hybrid setup, 2 brokers at the ELKH cloud, and 2 brokers at AWS.
6-6 and 12-12 producers sending messages, but to different topics:
- ELKH producers: to the 'ELKH' topic
- AWS producers: to the 'AWS' topic
